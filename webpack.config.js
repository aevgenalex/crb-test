const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const webpack = require('webpack');
const path = require('path');


const NODE_ENV = process.env.NODE_ENV || 'development';
const isProd = NODE_ENV == 'production';
console.log(NODE_ENV, `| is prod: ${isProd}`);

const plugins = [
  new webpack.optimize.UglifyJsPlugin(),
  new HtmlWebpackPlugin({
    template: './src/index.html',
    inlineSource: '.(js|css)$'
  }),
  new HtmlWebpackInlineSourcePlugin(),
  new CleanWebpackPlugin(['dist']),
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(NODE_ENV)
  }),
];

const config = {
  entry: path.resolve(__dirname, 'src/index.jsx'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/'
  },
  devtool: isProd ? '' : 'eval-cheap-module-source-map',
  devServer: {
    contentBase: path.resolve(__dirname, 'dist')
  },
  resolve: {
    modules: ['node_modules', path.resolve(__dirname, 'src')],
    extensions: ['.jsx', '.js', '.json'],
  },
  module: {
    rules: [
      { 
        test: /\.jsx?$/, 
        use: 'babel-loader',
        exclude: /node_modules/,
      },
      { 
        test: /\.css$/, 
        loaders: [
          `style-loader${isProd ? '' : '?sourceMap'}`,
          `css-loader?modules${isProd ? '' : '&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]'}`
        ]      
      }
    ]
  },
  plugins,
};

module.exports = config;