import React from 'react';
import css from './matrix.css';


const Domains = ({ domains }) => (
  <div>
    {/* {domains.map((domain, i) => (
      <div className={css.domain} key={i}>[
        {domain.map((cell, j) => 
          <span key={j}>
            ({cell.point[0]}, {cell.point[1]})
          </span>
        )}]
      </div>
    ))}
    <hr/> */}
    {domains.length}
  </div>
);

export const MatrixResult = ({ results = [] }) => (
  <div>
    <table width="100%">
      <thead>
        <tr>
          <th>Вероятность</th>
          <th>Домены</th>
          <th>Размер</th>
        </tr>
      </thead>
      <tbody>
        {results.map(({ chance, isCustom, domains, matrixSize, time }) => (
          <tr className={css.tr} key={time} >
            <td>
              {isCustom
                ? 'custom'
                : `${chance}%`
              }
            </td>
            <td><Domains domains={domains} /></td>
            <td>({matrixSize[0]}, {matrixSize[1]})</td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>
);
