import React from 'react';


const I_INDEX = 0;
const J_INDEX = 1;

export const MatrixForm = ({ 
  matrixSize, 
  chance, 
  onChange, 
  onGenerate,
  onCount,
  countDisabled,
  onAuto,
}) => {
  const [i, j] = matrixSize;
  const handleChangeChance = ({ target: { value } }) => onChange({ chance: value });
  const handleChangeSize = index => ({ target: { value }}) => { 
    const changed = [...matrixSize];
    changed[index] = value;
    onChange({ matrixSize: changed });
  }
  return (
    <div>
      <div>
        <fieldset>
          <label htmlFor="i-control">i</label>
          <input 
            onChange={handleChangeSize(I_INDEX)}
            value={i} 
            id="i-control" 
            type="number" 
            min="1" 
            max="38" 
          />

          <label htmlFor="j-control">j</label>
          <input 
            onChange={handleChangeSize(J_INDEX)}
            value={j} 
            id="j-control" 
            type="number" 
            min="1" max="38"
          />

          <label htmlFor="chance">%</label>
          <input 
            value={chance} 
            onChange={handleChangeChance} 
            type="number" 
            min="0" 
            max="100"
          />

          <button onClick={onGenerate}>Генерировать</button>

          <button disabled={!onAuto} onClick={onAuto}>Авто</button>

          <button onClick={onCount} disabled={countDisabled}>Подсчитать</button>
        </fieldset>
      </div>
    </div>
  );
};
