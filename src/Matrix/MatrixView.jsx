import React from 'react';
import css from './matrix.css';

const randomColor = () => Math.floor(Math.random() * 255);

export const MatrixView = ({ matrix, domains = [], onClick }) => (
  <div>
    <style>
      {domains.map((d, i) => (`
        .in-domain-${i + 1} {
          background-color: rgb(${randomColor()}, ${randomColor()}, ${randomColor()});
        }
      `))}
    </style>
    <table className={onClick ? css.clickable : css.unclickable}>
      <tbody>
        {matrix.map((row, i) => (
          <tr className={css.cell} key={i}>
            {row.map(({ value, domainId }, j) => (
              <td 
                onClick={onClick ? (() => onClick(i, j)) : undefined} 
                className={domainId ? `in-domain-${domainId}` : ''} 
                key={j}
              >
                {value ? 1 : 0}
                <small className={css.label}>
                  ({i}, {j})
              </small>
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  </div>
);