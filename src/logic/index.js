const getRandomValue = chance => chance
  ? Math.random() < chance ? true : false
  : false;

// work
export const generateMatrix = (size = [], chance = 0) => {
  const matrix = [];
  const [i, j] = size;
  let currentJ = 0;
  while (currentJ++ < j) {
    const row = [];
    matrix.push(row);
    let currentI = 0;
    while (currentI++ < i) {
      row.push(new Cell(getRandomValue(chance)));
    }
  }
  return matrix;
}

const Cell = function (value = false, visited = false) {
  this.value = value;
  this.visited = visited;
}

const getItem = (matrix, i, j) => {
  const row = matrix[i];
  if (!row) return undefined;
  return row[j];
}

const forEachMatrix = (matrix, callback) => matrix.forEach(
  (row, i) => row.forEach(
    (item, j) => callback({ matrix, i, j, item })
  )
);

export const getDomains = (matrix) => {
  const domains = [];

  const visitSiblings = (matrix, i, j, domain) => {
    const item = getItem(matrix, i, j);

    if (!item || item.visited) return;

    item.visited = true;
    if (item.value) {
      if (!domain) {
        domain = [];
        domains.push(domain);
      }
      item.point = [i, j];
      item.domainId = domains.length;
      domain.push(item);
      visitSiblings(matrix, i - 1, j, domain);
      visitSiblings(matrix, i + 1, j, domain);
      visitSiblings(matrix, i, j + 1, domain);
      visitSiblings(matrix, i, j - 1, domain);
    }
  }

  forEachMatrix(matrix, ({ matrix, i, j }) => visitSiblings(matrix, i, j));

  return domains;
}