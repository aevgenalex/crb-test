import React, { Component } from 'react';
import { MatrixForm, MatrixView, MatrixResult } from '../Matrix';
import { generateMatrix, getDomains } from '../logic/index';

import css from './style.css';


const MAX_RESULTS = 10;


export class App extends Component {
  state = {
    matrixSize: [10, 10],
    chance: 10,
    matrix: [],
    results: [],
    isCounted: false,
    isCustom: false,
    domains: [],
  }

  changeConfig = config => this.setState(config)

  generate = (then) => {
    const { matrixSize, chance } = this.state;
    this.setState({
      matrix: generateMatrix(matrixSize, chance / 100),
      isCounted: false,
      isCustom: false,
    }, then);
  }

  handleGenerate = () => {
    this.generate();
  }

  handleCount = () => {
    const { matrixSize, matrix, chance, results, isCustom } = this.state;
    const domains = getDomains(matrix);
    const newResults = [
      ...(results.length >= MAX_RESULTS ? results.slice(1) : results ),
      { matrixSize, chance, domains, isCustom, time: Date.now() },
    ];
    this.setState({
      isCounted: true,
      results: newResults,
      domains,
      matrix: [...matrix],
    })
  }

  handleAuto = () => this.generate(this.handleCount)

  handleCellClick = (i, j) => {
    const { matrix } = this.state;
    const item = matrix[i][j];
    matrix[i][j] = { ...item, value: !item.value };
    this.setState({
      matrix: [...matrix],
      isCustom: true,
    })
  }

  render() {
    const { matrixSize, results, matrix, chance, isCounted, domains, isCustom } = this.state;
    return (
      <div className={css.container}>
        <div className={css.row}>
          <div className={css.col}>
            <MatrixForm
              chance={chance}
              matrixSize={matrixSize}
              onChange={this.changeConfig}
              onGenerate={this.handleGenerate}
              onAuto={!isCustom ? this.handleAuto : undefined}
              countDisabled={isCounted}
              onCount={this.handleCount}
            />

            <MatrixView
              matrix={matrix} 
              domains={domains}
              onClick={!isCounted && this.handleCellClick}
            />
          </div>
          <div className={css.col}>
            <MatrixResult
              results={results}
            />
          </div>
        </div>
      </div>
    );
  }
}
